/**
    Manager of device inforation
    @file deviceinfomanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <Arduino.h>
/**
 * Gets device info as json string
 * @return json representation fo device basic information
 */
const char *getDeviceInfo();