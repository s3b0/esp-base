/**
    Manager for application settings
    It basicly read application data writen in JSON format and saves it if require.
    @file appdatamanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <ArduinoJson.h>
#include <LittleFS.h>
/**
 * Reads app json data from file
 * @return app data json file
 */
File readAppData();
/**
 * Saves app data to json file
 * @param app data as string(with json data)
 */
void saveAppData(const String appData);
/**
 * Removes app data file
 */
void resetAppData();
/**
 * Gets float value from app data file
 * @param jsonKey key of required value
 */
const float getFloatValue(const char *jsonKey);