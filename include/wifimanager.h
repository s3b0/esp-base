/**
    Manager for device wifi
    @file wifimanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <ESP8266WiFi.h>
/**
 * Sets up WIFI
 * @return true if wifi set up properly
 */
bool setupWiFi();
/**
 * Disconects wifi
 */
void disconnectWifi();
/**
 * Checks if wifi is online(AP) or connected (is STA)
 */
bool isWifiConnected();
/**
 * Clears ESP interna wifi cache
 */
void resetWifiSettings();
/**
 * Gets number of connected clients if in AP mode
 * @return only in AP mode: count of wifi clients
 */
int getConnectedClients();
/**
 * Gets our ip address
 * @return our ip address
 */
IPAddress getLocalIP();