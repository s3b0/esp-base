/**
    Utilities
    @file utils.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <ArduinoJson.h>
#include "printmanager.h"

/**
 * Check if string(char*) is empty or not
 * @param chars string to check
 * @return true if string empty
 */
bool isCharsEmpty(char *chars);
/**
 * Print json to serial
 * @param object json object to print
 */
void printJson(JsonObject object);
/**
 * Convert MAC from char* to string
 * @param mac mac to convert
 * @return converted mac
 */
String macToString(const unsigned char *mac);
/**
 * Convert boolean to string representation
 * @param boolean value to convert
 * @return string representation of bool, if true returns "true" and otherwise if other cases
 */
const char* booleanToString(const bool boolean);
/**
 * Gets int length (not size :) real length)
 * example: 100 has length = 3
 * @param x int to count its length
 * @return length of int
 */
const int intLength(const unsigned int x);