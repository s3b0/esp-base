#pragma once
/**
 * Severiyty of logs - in config.json can appear INFO, DEBUG, ERROR or WARNING. APPENDING is rserved for code only!
 */
enum LOG_SEVERITY {
    APPENDING,
    INFO,
    DEBUG,
    ERROR,
    WARNING
};
/**
 * Max sensor count
 */
#define SENSORS_MAX_COUNT 2
/**
 * Web log separator - you can split lines of web logs at client side
 */
#define WEB_LOG_LINE_SEPARATOR '|'
/**
 * Delay of builtin led - used only as error indicator
 */
#define LED_ERROR_DELAY 500
/**
 * Delay for web client when reading web logs
 */
#define WEB_LOGER_READ_DELAY 1000
/**
 * In exchage of normal delay() - we don't want to stop whole program(when using delay)
 */
#define SENSOR_READ_DELAY 1000
/**
 * Do we need web server?
 */
#define USE_WEBSERVER
/**
 * Do we need OTA
 */
//#define USE_OTA

/**
 * Resets all device settings!
 */
void reset();
/**
 * Returns configuration data for client to properly read web logs
 * Client must be synced wit us(this data will hel him)
 * @return sync data for client
 */
char *getWebLoggerInfo();