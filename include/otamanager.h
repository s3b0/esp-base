/**
    Manager for ota updates
    @file otamanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
/**
 * Sets up OTA
 */
void setupOTA();
/**
 * Handle ota updates in loop
 */
void handleOTA();