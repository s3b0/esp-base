/**
    Manager for device state
    @file actionmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
/**
 * Reboots device
 */
void reboot();
/** 
 * Will set MODEM_SLEEP mode that puts WiFi to off state when delay invoked
 * example: delay(1000); it will put WiFi off for 1s
 * RTC enable will be still avaible
 */
void setModemSleep();
/**
 * Will set LIGHT_SLEEP mode.
 * Light sleep makes CPU go to idle when you invoke delay.
 * example: delay(1000); <- it will put board into sleep on 1s
 * RTC enable will be still avaible
 */
void setLightSleep();
/**
 * Turns of WIFI, System clock, CPU to wakeup pin 16 must be connected to RST pin
 * RTC enable will be still avaible
 * More info:
 * The RST (Reset) pin is held high via a pull-up during the device’s active state. To reset the device the pin must be pulled to ground externally.
 * As soon as the watchdog timer is up (i.e. it’s time to wake up), a LOW signal is sent to GPIO16 (Pin D0). Thus, by wiring GPIO16 to RST,
 * the device is restarted as soon as the sleep timer is up.
*/
void deepSleep();
