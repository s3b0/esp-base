/**
    Manager of application print services
    @file printmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <Arduino.h>
#include "settingsmanager.h"
#include "configmanager.h"

/**
 * Sets application runtime log level
 * NOTE: Don't use in in printmanager.cpp! Otherwise device will hang!
 * @param logData application runtime log settings
 */
void setLogger(LOG_DATA logData);
/**
 * Prints float to serial and (optionaly) to web client
 * Internaly its invoking @link void print(String text, LOG_SEVERITY level = DEBUG, bool newLine = true);
 * @param number float number to print
 * @param level log level(severity)
 * @param newLine true if you want to end line with new line character
 */
void print(float number, LOG_SEVERITY level = DEBUG, bool newLine = true);
/**
 * Prints string to serial and (optionaly) to web client. Additionally when error occur it start blinking builtin LED with LED_ERROR_DELAY delay
 * @see settingsmanager.h
 * @param text text to print
 * @param level log level(severity)
 * @param newLine true if you want to end line with new line character
 */
void print(String text, LOG_SEVERITY level = DEBUG, bool newLine = true);
/**
 * Prints formated string to serial and (optionaly) to web client
 * @param format format for printed string
 * @param level log level(severity)
 * @param ... params for format
 */
void printFormat(const char *format, LOG_SEVERITY level, ...);
/**
 * Returns web logs
 * @return web logs for client
 */
const char *printWebLogs();
/**
 * Invoke this to start generating web logs after that you can gather web logs by invoking @link printWebLogs()
 */
void enablePrintingWebLogerForWebClient();