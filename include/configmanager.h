/**
    Manager for device state
    @file actionmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <Arduino.h>
#include "LittleFS.h"
#include "settingsmanager.h"
/**
 * Stores MQTT_DATA for runtime use
 */
struct MQTT_DATA
{
    char serverIp[16] = "";
    char serverName[60] = "";
    int mqttPort = 1883;
    char token[60] = "";
    char username[30] = "";
    char password[30] = "";
};
/**
 * Stores NET_DATA for runtime use
 * It will be used when WIFI will try to configure itself
 */
struct NET_DATA
{
    char staticIp[16] = "10.0.1.1";
    char staticGw[16] = "10.0.1.1";
    char staticNm[16] = "255.255.255.0";
};
/**
 * Stores WIFI_DATA for runtime use
 * In AP mode all params will be used
 * In STA mode only ssid and psk will be used
 * In STA+AP TODO
 */
struct WIFI_DATA
{
    String ssid = "esp";
    String psk = "12345678";
    char mode[4] = "AP"; //STA, AP
    int channel = 1;
    bool hidden = false;
    int max_wifi_client_count = 1;
};
/**
 * Stores LOG_DATA for runtime use
 * severityAsString is used when printing to serial or to web loger
 * NOTE: at the start all message are DEBUG leveled! it is not possible to change it
 * Level will change after config.json will be loaded @see configmanager.h
 */
struct LOG_DATA
{
    LOG_SEVERITY severity = DEBUG;
    char severityAsString[8] = "DEBUG";
    bool serialLogEnable = true;
};
/**
 * Stores SLEEP_DATA for runtime use
 * @param time is time after wich device will wakup itself from DEEP_SLEEP (only!)
 * @param mode mode of slee can be LIGHT, MODEM, DEEP
 * @param unit time unit of sleep time - onlu used for web client (sleep time is store/used in app in miliseconds,)
 */
struct SLEEP_DATA
{
    long time = 1000000; // 1 second
    char mode[6] = "DEEP";
    char unit[2] = "s"; // mode in seconds
};
/**
 * Gets filled with values (default or from config.json)
 * @return filled MQTT_DATA
 */
MQTT_DATA getMqttData();
/**
 * Gets filled with values (default or from config.json)
 * @return filled NET_DATA
 */
NET_DATA getNetData();
/**
 * Gets filled with values (default or from config.json)
 * @return filled WIFI_DATA
 */
WIFI_DATA getWifiData();
/**
 * Gets filled with values (default or from config.json)
 * @return filled LOG_DATA
 */
LOG_DATA getLogData();
/**
 * Gets filled with values (default or from config.json)
 * @return filled SLEEP_DATA
 */
SLEEP_DATA getSleepData();

/**
 * Checks if token in MQTT_DATA aviable
 * @param MQTT_DATA structure
 * @return true if token aviable(and has value)
 */
bool hasToken(MQTT_DATA mqttData);
/**
 * Checks if server name in MQTT_DATA aviable
 * @param MQTT_DATA structure
 * @return true if server name aviable(and has value)
 */
bool hasServerName(MQTT_DATA mqttData);
/**
 * Saves passed config to config.json
 * @param config jason as string
 */
void saveConfigData(const String configData);
/**
 * Reads config.json
 * @return config.json as file
 */
File readConfigData();
/**
 * Removes config.json file
 */
void resetConfig();
/*void createLock();
void releaseLock();
bool hasLock();*/