/**
    Manager for DHT sensors
    @file dhtmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <DHT.h>
#include "sensorsmanager.h"
/**
 * Data of DHT sensor
 * uses common structure for temerature and humidity from sensorsmanager.h @see sensorsmanager.h
 */
struct DhtData
{
  TemperatureSensor temperature;
  HumiditySensor humidity;
};
/**
 * Reads temperature and humidity from sensor and sets for this sensor id
 * @param dhtSensor pointer to dht sensor
 * @param sensorId sensor id to set
 * @return data with values @link DhtData
 */
DhtData readTempHumVal(DHT *dhtSensor, const int sensorId);
/**
 * Reads average temperature and humidity from 2 sensors
 * @param first pointer to dht first sensor
 * @param second pointer to dht second sensor
 * @return data with average values @link DhtData
 */
DhtData getAverageTempHum(const DhtData first, const DhtData second);