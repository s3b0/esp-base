/**
    Common sensor interface
    @file sensorsmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <Arduino.h>
/**
 * JSON key for temerature data (used in app.json), must be same as used in app.html
 */
#define JSON_TEMPERATURE_SENSOR_KEY "temperature"
/**
 * JSON key for humidity data (used in app.json), must be same as used in app.html
 */
#define JSON_HUMIDITY_SENSOR_KEY "humidity"

/**
 * Common struct for temperature sensor
 */
struct TemperatureSensor
{
    int id;
    float value = 0.0/0.0; // implicit set to NAN (if exlicit set warning: overflow in implicit constant conversion occurs)
    String name = "Temperature sensor";
    char unit = 'C';
    const char *type = JSON_TEMPERATURE_SENSOR_KEY;
    bool isAverage = false;
};
/**
 * Common struct for humidity sensor
 */
struct HumiditySensor
{
    int id;
    float value = 0.0/0.0; // implicit set to NAN (if exlicit set warning: overflow in implicit constant conversion occurs)
    String name = "Humidity sensor";
    char unit = '%';
    const char *type = JSON_HUMIDITY_SENSOR_KEY;
    bool isAverage = false;
};
/**
 * Returns string(as JSON) representation of all sensors data connected to device
 * @return json string with sensors value
 */
const String readSensorsData();
/**
 * Adds values of sensors(with apropriate structure) to interna array with that sensors
 * Size of interna array is set with SENSORS_MAX_COUNT const in settingsmanager.h @see settingsmanager.h
 * @param temperatureSensor with values to add to internal array
 */
void addValuesToTemperatureSensors(const TemperatureSensor temperatureSensor);
/**
 * Adds values of sensors(with apropriate structure) to interna array with that sensors
 * Size of interna array is set with SENSORS_MAX_COUNT const in settingsmanager.h @see settingsmanager.h
 * @param humiditySensor with values to add to internal array
 */
void addValuesToHumiditySensors(const HumiditySensor humiditySensor);
/**
 * Sets verage value of sensors(with apropriate structure)
 * @param temperatureSensor with average value
 */
void setAverageValueForTemperatureSensors(const TemperatureSensor temperatureSensor);
/**
 * Sets verage value of sensors(with apropriate structure)
 * @param humiditySensor with average value
 */
void setAverageValueForHumiditySensors(const  HumiditySensor humiditySensor);