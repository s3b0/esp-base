/**
    Wrapper for common file actions
    @file fsmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <LittleFS.h>
/**
 * Files system open modes
READ = r      Open text file for reading.  The stream is positioned at the
       beginning of the file.

READ_FROM_BEGINNING = r+     Open for reading and writing.  The stream is positioned at the
       beginning of the file.

WRITE = w      Truncate file to zero length or create text file for writing.
       The stream is positioned at the beginning of the file.

WRITE_FROM_BEGINNING = w+     Open for reading and writing.  The file is created if it does
       not exist, otherwise it is truncated.  The stream is
       positioned at the beginning of the file.

APPEND = a      Open for appending (writing at end of file).  The file is
       created if it does not exist.  The stream is positioned at the
       end of the file.

APPEDN_AT_BEGINNING = a+     Open for reading and appending (writing at end of file).  The
       file is created if it does not exist.  The initial file
       position for reading is at the beginning of the file, but
       output is always appended to the end of the file.
*/
enum FS_READ_MODE {
    READ,
    READ_FROM_BEGINNING, 
    WRITE, 
    WRITE_FROM_BEGINNING, 
    APPEND, 
    APPEDN_AT_BEGINNING
};
/**
 * Gets file by passed name
 * Opens file with name with givn mode, if requested file not exist empty File will be returned!
 * @param name file name to open
 * @param mode mode with which file will be opened
 */
File getFile(String name, FS_READ_MODE mode = READ);
/**
 * Just unmounting fs
 */
void unmountFs();
/**
 * Checks if file exist
 * @param name file name to check
 * @return true if file exist
 */
bool checkIfFileExist(String name);
/**
 * @link checkIfFileExist(String name)
 */
bool checkIfFileExist(const char *name);
/**
 * Removes file
 * @param name file name to remove
 * @return true if file has been successfully removed
 */
bool removeFile(const char *name);