/**
    Manager for web server
    @file webservermanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
#include <ESP8266WebServer.h>
/**
 * Sets up server and register all required web enpoints
 * @param webServerAddress web server IPv4 adress
 * @param dnsName name for server (will be used in MDNS resover)
 * @param port web port (TCP, will be used in MDNS resover too)
 */
bool setupWebServer(IPAddress webServerAddress, String dnsName, int port = 80);
/**
 * Use this if you want to refer to used web server
 */
ESP8266WebServer *getWebServer();
/**
 * !!!Handle with care!!!
 * Shuts down web server - after that any try of using web server will cause chip to reset!
 * Use only for shutdown power(reboot) actions!
 * Frees pointer to actual used web server!
 */
void shutDownWebServer();