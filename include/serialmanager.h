/**
    Manager for serial logger
    @file serialmanager.h
    @author Sebastian Krupa <s3b0@mail.com>
    @version 0.1
*/
#pragma once
/**
 * Sets up serial
 */
void serialBegin();
/**
 * Ends serial
 */
void serialEnd();

