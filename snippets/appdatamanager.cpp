#include "appdatamanager.h"
#include <ArduinoJson.h>
#include <fsmanager.h>
#include <printmanager.h>

#define APP_FILE_SIZE_IN_BYTES 800
#define APP_DATA_OBJECT_NAME "APP_DATA"
// Fixed name of configuration file
#define APP_DATA_FILE "/app.json"

#define NO_DATA_ERROR_MSG "{\"ERROR\" : \"Check logs!\"}"

void setPrimitiveFromJsonObjectValue(JsonObject &jsonObject, JsonObject &jsonToSave);
void setPrimitiveJson(JsonPair keyValue, JsonObject &jsonToSave);

File readAppData()
{
    File appFile; // App data file, if null not loaded
    // if no configuration - make new one(empty)
    while (!(appFile = getFile(APP_DATA_FILE, READ)))
    {
        saveAppData("{\"" + String(APP_DATA_OBJECT_NAME) + "\":{}}");
    }

    /* //do not need json parsin - file is JSON! :)
    DynamicJsonDocument jsonBuffer(APP_FILE_SIZE_IN_BYTES);
    auto error = deserializeJson(jsonBuffer, appFile);
    if (!error)
    {
        if (jsonBuffer.containsKey(APP_DATA_OBJECT_NAME))
        {
            JsonObject ad = jsonBuffer[APP_DATA_OBJECT_NAME]; // App data
            String str;
            serializeJson(ad, str);
            printFormat("Loaded app data from file %s, data: %s", DEBUG, APP_DATA_FILE, str.c_str());
            return str;
        }
        else
            print("No app data in app data config!", ERROR);
    }*/
    if (appFile) //much cheaper :)!
        printFormat("Loaded app data from file %s", DEBUG, APP_DATA_FILE);
    else
        printFormat("Failed to load %s", ERROR, APP_DATA_FILE);
    return appFile;
}

void saveAppData(const String appData)
{
    File saveFile = getFile(APP_DATA_FILE, WRITE);

    if (!appData.isEmpty())
    {
        printFormat("Saving app data: %s", DEBUG, appData.c_str());
        const int addDataFileSize = appData.length() + 1;
        DynamicJsonDocument jsonAppDataDoc(addDataFileSize);
        DynamicJsonDocument jsonToSaveDoc(addDataFileSize);
        JsonObject jsonToSave = jsonToSaveDoc.createNestedObject(APP_DATA_OBJECT_NAME);
        deserializeJson(jsonAppDataDoc, appData);
        for (JsonPair keyValue : jsonAppDataDoc.as<JsonObject>())
        {
            auto value = keyValue.value();
            if (value.is<JsonObject>())
            {
                print("Got json object - pushing for primitives");
                JsonObject jsonObjectValue = value.as<JsonObject>();
                setPrimitiveFromJsonObjectValue(jsonObjectValue, jsonToSave);
            }
            else
            {
                setPrimitiveJson(keyValue, jsonToSave);
            }
        }
        serializeJson(jsonToSaveDoc, saveFile);
        print("App data saved.");
    }
    saveFile.close();
}

void setPrimitiveFromJsonObjectValue(JsonObject &jsonObject, JsonObject &jsonToSave)
{
    for (JsonPair keyValue : jsonObject)
    {
        setPrimitiveJson(keyValue, jsonToSave);
    }
}

void setPrimitiveJson(JsonPair keyValue, JsonObject &jsonToSave)
{
    const char *key = keyValue.key().c_str();
    auto value = keyValue.value();
    if (value.is<int>())
    {
        const int intValue = value.as<int>();
        printFormat("Got key %s and int value %d", DEBUG, key, intValue);
        jsonToSave[key] = intValue;
    }
    else if (value.is<const char *>())
    {
        const char *charValue = value.as<const char *>();
        printFormat("Got key %s and string value %s", DEBUG, key, charValue);
        jsonToSave[key] = charValue;
    }
    else if (value.is<bool>())
    {
        const bool boolValue = value.as<bool>();
        printFormat("Got key %s and bool value %s", DEBUG, key, boolValue);
        jsonToSave[key] = boolValue;
    }
    else
    {
        printFormat("Got key %s - setting value as is", DEBUG, key);
        jsonToSave[key] = value;
    }
}