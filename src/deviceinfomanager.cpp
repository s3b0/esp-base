#include "deviceinfomanager.h"
#include "utils.h"
/**
 * Format for device info json object - if changig change javascript object in config.html also!
 */
#define DEVICE_INFO_JSON_DATA "{\"DEVICE_INFO\": {\"chipId\":%zu, \"coreVersion\":\"%s\", \"cputFreqMhz\":%zu, \"flashChipId\":%zu, \"flashSize\":%zu, \"flashSpeed\":%zu, \"freeHeap\":%zu, \"heapMax\":%zu, \"heapFragmentation\":%zu, \"fullVersion\":\"%s\", \"resetInfo\":\"%s\", \"resetReason\":\"%s\", \"sdkVersion\":\"%s\", \"sketchSize\":%zu, \"vcc\":%zu}}"

const char *getDeviceInfo()
{
    const uint32_t chipId = ESP.getChipId();
    String coreVersion = ESP.getCoreVersion();
    const uint8_t cputFreqMhz = ESP.getCpuFreqMHz();
    const uint32_t flashChipId = ESP.getFlashChipId();
    const uint32_t flashSize = ESP.getFlashChipRealSize();
    const uint32_t flashSpeed = ESP.getFlashChipSpeed();
    uint32_t *freeHeap = (uint32_t*)malloc(sizeof(uint32_t));
    uint16_t *heapMax = (uint16_t*)malloc(sizeof(uint16_t));
    uint8_t *heapFragmentation = (uint8_t*)malloc(sizeof(uint8_t));
    ESP.getHeapStats(freeHeap, heapMax, heapFragmentation);
    String fullVersion = ESP.getFullVersion();
    String resetInfo = ESP.getResetInfo();
    String resetReason = ESP.getResetReason();
    const char *sdkVersion = ESP.getSdkVersion();
    const uint32_t sketchSize = ESP.getSketchSize();
    uint16_t vcc = ESP.getVdd33();

    const int jsonLength = strlen(DEVICE_INFO_JSON_DATA) + intLength(chipId) + coreVersion.length() + intLength(cputFreqMhz) + intLength(flashChipId)
    + intLength(flashSize) + intLength(flashSpeed) + intLength(*freeHeap) + intLength(*heapMax) + intLength(*heapFragmentation) + fullVersion.length()
    + resetInfo.length() + resetReason.length() + strlen(sdkVersion) +  + intLength(sketchSize) +  + intLength(vcc);
    char *jsonObject = (char *)malloc(jsonLength * sizeof(char));

    sprintf(jsonObject, DEVICE_INFO_JSON_DATA, chipId, coreVersion.c_str(), cputFreqMhz, flashChipId, flashSize, flashSpeed, *freeHeap, 
    *heapMax, *heapFragmentation, fullVersion.c_str(), resetInfo.c_str(), resetReason.c_str(), sdkVersion, sketchSize, vcc);

    free(freeHeap);
    free(heapMax);
    free(heapFragmentation);

    return jsonObject;
}