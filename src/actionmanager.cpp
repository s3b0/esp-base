#include "actionmanager.h"
#include "printmanager.h"
#include "wifimanager.h"
#include "configmanager.h"
#include "fsmanager.h"
#include "webservermanager.h"
#include "serialmanager.h"

void prepare()
{
    disconnectWifi();
    shutDownWebServer();
    unmountFs();
    serialEnd();
}

void reboot()
{
    print("Rebooting device", INFO);
    prepare();
    ESP.restart();
}

void setModemSleep()
{
    WIFI_DATA wifiData = getWifiData();
    if (strcmp(wifiData.mode, "STA"))
    {
        print("Light sleep enabled - shuting down WiFi", INFO);
        //wifi_set_sleep_type(MODEM_SLEEP_T);
    }
    else
    {
        print("WiFi in AP mode - can't enable modem sleep mode.", WARNING);
    }
}

void setLightSleep()
{
    WIFI_DATA wifiData = getWifiData();
    if (strcmp(wifiData.mode, "STA"))
    {
        print("Light sleep enabled - shuting down WiFi and system clock. Setting CPU idle on given time(in delay) when delay() invoked.", INFO);
        // wifi_set_sleep_type(LIGHT_SLEEP_T);
    }
    else
    {
        print("WiFi in AP mode - can't enable light sleep mode.", WARNING);
    }
}

void deepSleep()
{
    SLEEP_DATA sleepData = getSleepData();
    printFormat("Deep sleep enabled - going to sleep for %d milliseconds, to wakup wire GPIO 16 and RST", INFO, sleepData.time);
    prepare();
    ESP.deepSleep(sleepData.time);
}
