#include "settingsmanager.h"
#include "configmanager.h"
#include "printmanager.h"
#include "fsmanager.h"
#include "utils.h"
#include <LittleFS.h>

// Fs mount state indicator
bool isFsMounted = false;

bool mountFs();

File getFile(String name, FS_READ_MODE mode)
{
    File file;
    if (mountFs()) //do not check file existance - it is possible that fill will not exist!
    {
        switch (mode)
        {
        case READ:
            printFormat("Opening file %s for read", DEBUG, name.c_str());
            file = LittleFS.open(name, "r");
            break;
        case READ_FROM_BEGINNING:
            printFormat("Reading from beginning of %s", DEBUG, name.c_str());
            file = LittleFS.open(name, "r+");
            break;
        case WRITE:
            printFormat("Writing to %s", DEBUG, name.c_str());
            file = LittleFS.open(name, "w");
            break;
        case WRITE_FROM_BEGINNING:
            printFormat("Writing to %s from beginning", DEBUG, name.c_str());
            file = LittleFS.open(name, "w+");
            break;
        case APPEND:
            printFormat("Appending to %s", DEBUG, name.c_str());
            file = LittleFS.open(name, "a");
            break;
        case APPEDN_AT_BEGINNING:
            printFormat("Appending to %s from beginning", DEBUG, name.c_str());
            file = LittleFS.open(name, "a+");
            break;
        }
    }
    return file;
}
/**
 * Mounts interna fs if needed
 */
bool mountFs()
{
    if (!isFsMounted)
    {
        print("Mounting fs...", DEBUG);
        if (LittleFS.begin())
        {
            print("Fs mounted", DEBUG);
            isFsMounted = true;
            return isFsMounted;
        }
        print("Error mounting fs", ERROR);
    }
    return isFsMounted;
}

void unmountFs()
{
    if (isFsMounted)
    {
        print("Unmounting fs...", DEBUG);
        LittleFS.end();
        isFsMounted = false;
        print("Fs unmounted", DEBUG);
    }
}

bool checkIfFileExist(String name)
{
    return checkIfFileExist(name.c_str());
}

bool checkIfFileExist(const char *name)
{
    if (mountFs())
    {
        if (LittleFS.exists(name))
        {
            return true;
        }
        printFormat("File %s doesn't exist", WARNING, name);
    }
    return false;
}

bool removeFile(const char *name)
{
    if (mountFs())
    {
        printFormat("Removing file %s", DEBUG, name);
        return LittleFS.remove(name);
    }
    return false;
}