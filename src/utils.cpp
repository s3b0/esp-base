#include "utils.h"

bool isCharsEmpty(char *chars)
{
    return (!chars) || (chars[0] == '\0');
}

void printJson(JsonObject object)
{
    String str;
    serializeJson(object, str);
    print(str);
}

String macToString(const unsigned char *mac)
{
    char buf[20];
    snprintf(buf, sizeof(buf), "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    return String(buf);
}

const char *booleanToString(const bool boolean)
{
    char *tmp;
    if (boolean)
    {
        tmp = (char *)malloc(5);
        strcpy(tmp, "true\0");
        return tmp;
    }
    tmp = (char *)malloc(6);
    strcpy(tmp, "false\0");
    return tmp;
}

const int intLength(const unsigned int x)
{
    if (x >= 1000000000)
        return 10;
    if (x >= 100000000)
        return 9;
    if (x >= 10000000)
        return 8;
    if (x >= 1000000)
        return 7;
    if (x >= 100000)
        return 6;
    if (x >= 10000)
        return 5;
    if (x >= 1000)
        return 4;
    if (x >= 100)
        return 3;
    if (x >= 10)
        return 2;
    return 1;
}