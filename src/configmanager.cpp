#include "configmanager.h"
#include "printmanager.h"
#include "fsmanager.h"
#include "utils.h"
#include <ArduinoJson.h>

#define CONFIG_FILE_SIZE_IN_BYTES 800
// Fixed name of configuration file
#define CONFIG_FILE_NAME "/config.json"
// Fixed lock name
//const char *lockFileName = "/.lock";
// do we have lock file?
//bool isLockFile = false;
// Data for mqtt comunication
struct MQTT_DATA mqttData;
// Data for network configuration
struct NET_DATA netData;
// Data for Wifi config
struct WIFI_DATA wifiData;
// Data for logeer
struct LOG_DATA logData;
// DAta for sleep mode
struct SLEEP_DATA sleepData;

// Config init flag - after read of configuration will be initilized with false
bool configLoaded = false;

void readConfig();

// Check if MQTT_DATA has token set
bool hasToken(MQTT_DATA mqttData)
{
    if (!isCharsEmpty(mqttData.token))
        return true;
    return false;
}
// Check if MQTT_DATA has server name set
bool hasServerName(MQTT_DATA mqttData)
{
    if (!isCharsEmpty(mqttData.serverName))
        return true;
    return false;
}
// Gets MQTT_DATA for application
MQTT_DATA getMqttData()
{
    readConfig();
    return mqttData;
}
// Serialize MQTT_DATA
void serialializeMqttData(DynamicJsonDocument *root)
{
    JsonObject mqttJsonData = root->createNestedObject("MQTT_DATA");
    mqttJsonData["serverIp"] = mqttData.serverIp;
    mqttJsonData["serverName"] = mqttData.serverName;
    mqttJsonData["mqttPort"] = mqttData.mqttPort;
    mqttJsonData["token"] = mqttData.token;
    mqttJsonData["username"] = mqttData.username;
    mqttJsonData["password"] = mqttData.password;
}
// GETS NET_DATA
NET_DATA getNetData()
{
    readConfig();
    return netData;
}
// Serialize NET_DATA
void serialializeNetData(DynamicJsonDocument *root)
{
    JsonObject netJsonData = root->createNestedObject("NET_DATA");
    netJsonData["staticIp"] = netData.staticIp;
    netJsonData["staticGw"] = netData.staticGw;
    netJsonData["staticNm"] = netData.staticNm;
}
// Gets Wifi data
WIFI_DATA getWifiData()
{
    readConfig();
    return wifiData;
}
// Serialize NET_DATA
void serialializeWifiData(DynamicJsonDocument *root)
{
    JsonObject netJsonData = root->createNestedObject("WIFI_DATA");
    netJsonData["ssid"] = wifiData.ssid;
    netJsonData["psk"] = wifiData.psk;
    netJsonData["mode"] = wifiData.mode;
    netJsonData["channel"] = wifiData.channel;
    netJsonData["hidden"] = wifiData.hidden;
    netJsonData["max_wifi_client_count"] = wifiData.max_wifi_client_count;
}
// Gets Log data
LOG_DATA getLogData()
{
    readConfig();
    return logData;
}
// Serialize NET_DATA
void serialializeLogData(DynamicJsonDocument *root)
{
    JsonObject logJsonData = root->createNestedObject("LOG_DATA");
    String severity = "DEBUG";
    switch (logData.severity)
    {
    case INFO:
        severity = "INFO";
        break;
    case ERROR:
        severity = "ERROR";
        break;
    case WARNING:
        severity = "WARNING";
        break;
    default: //only to stop compilation warning!
        break;
    }
    logJsonData["severity"] = severity;
    logJsonData["serialLogEnable"] = logData.serialLogEnable;
}
// Gets sleep data
SLEEP_DATA getSleepData()
{
    readConfig();
    return sleepData;
}
// Serialize NET_DATA
void serialializeSleepData(DynamicJsonDocument *root)
{
    JsonObject sleepJsonData = root->createNestedObject("SLEEP_DATA");
    sleepJsonData["mode"] = sleepData.mode;
    sleepJsonData["time"] = sleepData.time;
    sleepJsonData["unit"] = sleepData.unit;
}

// Saves default config
void saveDefaultConfig()
{
    print("Saving confugration...");
    File saveFile = getFile(CONFIG_FILE_NAME, WRITE);
    if (saveFile)
    {
        DynamicJsonDocument jsonBuffer(CONFIG_FILE_SIZE_IN_BYTES);
        serialializeMqttData(&jsonBuffer);
        serialializeNetData(&jsonBuffer);
        serialializeWifiData(&jsonBuffer);
        serialializeLogData(&jsonBuffer);
        serialializeSleepData(&jsonBuffer);
        serializeJson(jsonBuffer, saveFile);
    }
    else
        printFormat("Dont have file %s to save config!", ERROR, CONFIG_FILE_NAME);
    saveFile.close();
    // Config operation! unmount!
    unmountFs();
    configLoaded = false;
    print("Config saved");
}

// Gets config File
File readConfigData()
{
    // Config file
    File configFile;
    // if no configuration - make new one(empty)
    while (!(configFile = getFile(CONFIG_FILE_NAME, READ)))
    {
        saveDefaultConfig();
    }
    if (configFile)
        printFormat("Config file %s loaded", DEBUG, CONFIG_FILE_NAME);
    else
        printFormat("Config file %s not loaded!", ERROR, CONFIG_FILE_NAME);
    return configFile;
}
// Saves JSON to file
void saveConfigData(const String configData)
{
    if (!configData.isEmpty())
    {
        print("Saving confugration...");
        File saveFile = getFile(CONFIG_FILE_NAME, WRITE);
        saveFile.print(configData);
        saveFile.flush();
        saveFile.close();
        // Fs operation! unmount if unneeded!
        unmountFs();
        print("Config saved");
        configLoaded = false; //mark loaded config as dirty - needs to be reloaded!
    }
}
// Reads configuration and intilize with it all configuration structures for later use
void readConfig()
{
    if (!configLoaded) //load only if needed
    {
        File configFile = readConfigData(); //load config json file
        if (configFile)
        {
            DynamicJsonDocument jsonBuffer(CONFIG_FILE_SIZE_IN_BYTES);
            auto error = deserializeJson(jsonBuffer, configFile);
            if (!error)
            {
                print("Config parsed");
                if (jsonBuffer.containsKey("MQTT_DATA"))
                {
                    print("Setting mqtt data from config");
                    JsonObject md = jsonBuffer["MQTT_DATA"];
                    strcpy(mqttData.serverIp, md["serverIp"]);
                    strcpy(mqttData.serverName, md["serverName"]);
                    mqttData.mqttPort = md["mqttPort"].as<int>();
                    strcpy(mqttData.token, md["token"]);
                    strcpy(mqttData.username, md["username"]);
                    strcpy(mqttData.password, md["password"]);

                    printJson(md);
                }
                else
                {
                    print("No mqtt data in config");
                }

                if (jsonBuffer.containsKey("NET_DATA"))
                {
                    print("Setting custom ip from config");
                    JsonObject nd = jsonBuffer["NET_DATA"];
                    strcpy(netData.staticIp, nd["staticIp"]);
                    strcpy(netData.staticGw, nd["staticGw"]);
                    strcpy(netData.staticNm, nd["staticNm"]);

                    printJson(nd);
                }
                else
                {
                    print("No custom ip in config");
                }

                if (jsonBuffer.containsKey("WIFI_DATA"))
                {
                    print("Setting wifi data from config");
                    JsonObject wd = jsonBuffer["WIFI_DATA"];
                    wifiData.ssid = wd["ssid"].as<const char *>();
                    wifiData.psk = wd["psk"].as<const char *>();
                    strcpy(wifiData.mode, wd["mode"]);
                    wifiData.channel = wd["channel"].as<int>();
                    wifiData.hidden = wd["hidden"].as<bool>();
                    wifiData.max_wifi_client_count = wd["max_wifi_client_count"].as<int>();

                    printJson(wd);
                }
                else
                {
                    print("No wifi data in config");
                }
                if (jsonBuffer.containsKey("LOG_DATA"))
                {
                    print("Setting log data from config");
                    JsonObject ld = jsonBuffer["LOG_DATA"];
                    strcpy(logData.severityAsString, ld["severity"].as<const char *>());
                    if (strcmp(logData.severityAsString, "INFO") == 0)
                        logData.severity = INFO;
                    else if (strcmp(logData.severityAsString, "ERROR") == 0)
                        logData.severity = ERROR;
                    else if (strcmp(logData.severityAsString, "WARNING") == 0)
                        logData.severity = WARNING;
                    else
                        logData.severity = DEBUG;
                    logData.serialLogEnable = ld["serialLogEnable"].as<bool>();

                    printJson(ld);
                }
                else
                {
                    print("No log data in config");
                }
                if (jsonBuffer.containsKey("SLEEP_DATA"))
                {
                    print("Setting sleep data from config");
                    JsonObject sd = jsonBuffer["SLEEP_DATA"];
                    strcpy(sleepData.mode, sd["mode"].as<const char *>());
                    sleepData.time = sd["time"].as<long>();
                    strcpy(sleepData.unit, sd["unit"]);

                    printJson(sd);
                }
                else
                {
                    print("No sleep data in config");
                }
                // mark config loaded
                configLoaded = true;
                // Fs operation! unmount if unneeded!
                unmountFs();
                print("Config loaded.", INFO);
            }
            else
            {
                print("Failed to load config", ERROR);
                print(error.c_str(), ERROR);
            }
        }
    }
}

void resetConfig()
{
    print("Removing config file.");
    removeFile(CONFIG_FILE_NAME);
}
/*
void createLock()
{
    if (!checkIfFileExist(lockFileName))
    {
        print("Creating locking file...");
        File lockFile = getFile(lockFileName, WRITE);
        lockFile.close();
        // Fs operation! unmount if unneeded!
        unmountFs();
        isLockFile = true;
    }
}

void releaseLock()
{
    if (checkIfFileExist(lockFileName))
    {
        print("Releaseing locking file...");
        removeFile(lockFileName);
        // Fs operation! unmount if unneeded!
        unmountFs();
        isLockFile = false;
    }
}

bool hasLock()
{
    if (!isLockFile) //don't have lock, check
    {
        print("Found locking file...");
        isLockFile = checkIfFileExist(lockFileName);
        // Fs operation! unmount if unneeded!
        unmountFs();
    }
    return isLockFile;
}*/