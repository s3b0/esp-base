#include "appdatamanager.h"
#include "fsmanager.h"
#include "printmanager.h"
#include <ArduinoJson.h>

/**
 * App data json object name - must be equal with name of obejct from app.html
 */
#define APP_DATA_OBJECT_NAME "APP_DATA"
/**
 * File name of app data json file
 */
#define APP_DATA_FILE "/app.json"

File readAppData()
{
    File appDataFile;
    // if no configuration - make new one(empty)
    while (!(appDataFile = getFile(APP_DATA_FILE, READ)))
    {
        saveAppData("{\"" + String(APP_DATA_OBJECT_NAME) + "\":{}}");
    }
    if (appDataFile) //much cheaper then parsing JSON :)!
    {
        printFormat("Loaded app data from file %s", DEBUG, APP_DATA_FILE);
    }
    else
        printFormat("Failed to load %s", ERROR, APP_DATA_FILE);
    return appDataFile;
}

void saveAppData(const String appData)
{
    File saveFile = getFile(APP_DATA_FILE, WRITE);
    printFormat("Saving app data: %s", DEBUG, appData.c_str());
    saveFile.print(appData); //much cheaper then parsing JSON :)!
    saveFile.flush();
    print("App data saved.");
    saveFile.close();
}

void resetAppData()
{
    print("Removing app data file.");
    removeFile(APP_DATA_FILE);
}

const float getFloatValue(const char *jsonKey)
{
    File appDataFile = readAppData();
    const int appDataFileLength = appDataFile.size();
    printFormat("App data file size = %d", DEBUG, appDataFileLength);
    DynamicJsonDocument jsonDoc(appDataFileLength*2);
    auto error = deserializeJson(jsonDoc, appDataFile);
    float value = NAN;
    if (!error)
    {
        print("App data parsed");
        if (jsonDoc.containsKey(APP_DATA_OBJECT_NAME))
        {
            JsonObject ad = jsonDoc[APP_DATA_OBJECT_NAME];
            if (ad.containsKey(jsonKey))
            {
                if (ad[jsonKey].is<float>() || ad[jsonKey].is<int>())
                {
                    value = ad[jsonKey].as<float>();
                }
                else
                {
                    printFormat("App data value for key %s is not a float!", ERROR, jsonKey);
                }
            }
            else
            {
                printFormat("App data don't have key %s!", ERROR, jsonKey);
            }
        }
    }
    else
    {
        printFormat("Failed to parse app data, %s", ERROR, appDataFile.readString().c_str());
        print(error.c_str(), ERROR);
    }
    jsonDoc.clear();
    appDataFile.close();
    return value;
}