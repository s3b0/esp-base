#include "otamanager.h"
#include "printmanager.h"
#include "fsmanager.h"
#include <wifimanager.h>
#include <ArduinoOTA.h>
#include "utils.h"

// setup OTA
void setupOTA()
{
    // Port defaults to 8266
    // ArduinoOTA.setPort(8266);

    // Hostname defaults to esp8266-[ChipID]
    // ArduinoOTA.setHostname("myesp8266");

    // No authentication by default
    // ArduinoOTA.setPassword("admin");

    // Password can be set with it's md5 value as well
    // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
    // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");
    if (isWifiConnected())
    {
        ArduinoOTA.onStart([]() {
            String type;
            if (ArduinoOTA.getCommand() == U_FLASH)
            {
                type = "sketch";
            }
            else
            { // U_FS
                type = "filesystem";
            }
            // NOTE: if updating FS this would be the place to unmount FS using FS.end()
            unmountFs();
            print("Start updating " + type, DEBUG);
        });
        ArduinoOTA.onEnd([]() {
            print("\nEnd");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            printFormat("Progress: %u%%\r", INFO, (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            printFormat("Error[%u]: ", INFO, error);
            if (error == OTA_AUTH_ERROR)
            {
                print("Auth Failed", ERROR);
            }
            else if (error == OTA_BEGIN_ERROR)
            {
                print("Begin Failed", ERROR);
            }
            else if (error == OTA_CONNECT_ERROR)
            {
                print("Connect Failed", ERROR);
            }
            else if (error == OTA_RECEIVE_ERROR)
            {
                print("Receive Failed", ERROR);
            }
            else if (error == OTA_END_ERROR)
            {
                print("End Failed", ERROR);
            }
        });
        ArduinoOTA.begin();
    }
    else
    {
        print("Wifi not connected! OTA disabled!");
    }
}

// handle OTA
void handleOTA()
{
    if (isWifiConnected())
    {
        ArduinoOTA.handle();
    }
}