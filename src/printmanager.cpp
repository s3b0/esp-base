#include "settingsmanager.h"
#include "printmanager.h"
#include "utils.h"
#include "configmanager.h"
#include <Arduino.h>

#define WEB_LOGS_FORMAT "\"%s\" %c"

LOG_DATA AppLog;
unsigned long previousLedMillis = 0;
unsigned long previousWebLogClientRequestMillis = 0;
int ledState = LOW;

char *WebLogs;
bool clientRequestedWebLoger = false;

void setLogger(LOG_DATA logData)
{
    AppLog.serialLogEnable = logData.serialLogEnable;
    if (AppLog.severity != logData.severity && AppLog.serialLogEnable)
    {
        printFormat("Log level changed from %s to %s\n", INFO, AppLog.severityAsString, logData.severityAsString);
        digitalWrite(LED_BUILTIN, HIGH);
    }
    AppLog.severity = logData.severity;
}
/**
 * All function depends on this one - don't try to use other functions or loop of death occurs ;) (device will habg and will keep reset itself!)
 * @param text string text to print
 * @param newLine = true if new line at the end of line expcted
 */
void print(String text, bool newLine)
{
    if (!text.isEmpty())
    {
        if (Serial && AppLog.serialLogEnable)
        {
            if (newLine)
                Serial.println(text);
            else
                Serial.print(text);
        }
        //if web client have not been seen for longer than WEB_LOGER_READ_DELAY * n than disable WebLogger
        if (clientRequestedWebLoger && (millis() - previousWebLogClientRequestMillis >= WEB_LOGER_READ_DELAY * 2))
        {
            clientRequestedWebLoger = false; //client disconected!
            if (Serial && AppLog.serialLogEnable && (AppLog.severity == DEBUG || AppLog.severity == INFO))
                Serial.println("INFO: Client keep-alive timeout pass, disabling WebLoger.");
            if (WebLogs)
            {
                if (Serial &&  AppLog.serialLogEnable && AppLog.severity == DEBUG)
                    Serial.println("DEBUG: Freeing WebLogs mememory");
                free(WebLogs);
                WebLogs = NULL;
            }
        }
        else if (clientRequestedWebLoger)
        {
            int logLength = text.length() + 2;
            char *logLine = (char *)malloc(logLength * sizeof(char));
            if (logLine != NULL)
            {
                strcpy(logLine, text.c_str());
                logLine[logLength - 2] = WEB_LOG_LINE_SEPARATOR;
                logLine[logLength - 1] = '\0';

                if (WebLogs == NULL) //dont keep logs in memory forever! will be freed after every client request
                {
                    WebLogs = (char *)malloc(logLength);
                    if (WebLogs)
                        strcpy(WebLogs, logLine); //init cpy
                    else
                    {
                        if (Serial && AppLog.serialLogEnable)
                            Serial.println("ERROR: Cannot malloc memory for WebLogs!");
                        WebLogs = NULL;
                    }
                }
                else
                {
                    WebLogs = (char *)realloc(WebLogs, logLength + strlen(WebLogs));
                    if (WebLogs)
                        strcat(WebLogs, logLine); //add antother line
                    else
                    {
                        if (Serial && AppLog.serialLogEnable)
                            Serial.println("ERROR: Cannot realloc memory for WebLogs!");
                        WebLogs = NULL;
                    }
                }
                free(logLine);

                /*if (memoryFreed && WebLogs != NULL)
                {
                memoryFreed = false;
                const char *WebLogsMemoryFreeWebMessage = "Memory of webloger freed.\0";
                logLength = strlen(WEB_LOGS_FORMAT) + strlen(WebLogsMemoryFreeWebMessage) + 1;
                logLine = (char *)malloc(logLength * sizeof(char));
                sprintf(logLine, WEB_LOGS_FORMAT, WebLogsMemoryFreeWebMessage);
                strcat(WebLogs, logLine);
                if (logLine != NULL)
                    free(logLine);
                }*/
            }
            else
            {
                if (Serial && AppLog.serialLogEnable)
                    Serial.println("ERROR: Cannot malloc memory for logLine!");
            }
        }
    }
}

void print(String text, LOG_SEVERITY level, bool newLine)
{
    String log;
    unsigned long currentMillis = millis();
    switch (level)
    {
    case INFO:
        if (AppLog.severity == DEBUG || AppLog.severity == INFO)
            log = "INFO: " + text;
        break;
    case DEBUG:
        if (AppLog.severity == DEBUG)
            log = "DEBUG: " + text;
        break;
    case ERROR:
        log = "ERROR: " + text;
        if (currentMillis - previousLedMillis >= LED_ERROR_DELAY)
        {
            // save the last time you blinked the LED
            previousLedMillis = currentMillis;
            // if the LED is off turn it on and vice-versa:
            if (ledState == LOW)
            {
                ledState = HIGH;
            }
            else
            {
                ledState = LOW;
            }
            //inversion of signal LOW enable HIGS disable
            digitalWrite(LED_BUILTIN, ledState);
        }
        break;
    case WARNING:
        if (AppLog.severity == DEBUG || AppLog.severity == INFO || AppLog.severity == WARNING)
            log = "WARNING: " + text;
        break;
    default:
        log = text;
        break;
    }
    if (!log.isEmpty())
        print(log, newLine);
}

void printFormat(const char *format, LOG_SEVERITY level, ...)
{
    char temp[64];
    va_list arguments;
    va_start(arguments, level);
    size_t len = vsnprintf(temp, sizeof(temp), format, arguments);
    va_end(arguments);

    const int bufferLength = sizeof(char) * (len + 1);
    char *buffer = (char *)malloc(bufferLength);
    va_start(arguments, level);
    vsnprintf(buffer, bufferLength, format, arguments);
    va_end(arguments);
    print(buffer, level);
    free(buffer);
}
void print(float number, LOG_SEVERITY level, bool newLine)
{
    print(String(number), level, newLine);
}

const char *printWebLogs()
{
    char *tmp;
    if (WebLogs != NULL && clientRequestedWebLoger)
    {
        tmp = (char *)malloc(strlen(WebLogs) + 1);
        strcpy(tmp, WebLogs);
        if (Serial && AppLog.serialLogEnable && AppLog.severity == DEBUG)
            Serial.println("DEBUG: Freeing WebLogs mememory");
        free(WebLogs); //send it to client and forget!
        WebLogs = NULL;
    }
    else
    {
        tmp = (char *)malloc(4);
        strcpy(tmp, "...");
    }
    return tmp;
}

void enablePrintingWebLogerForWebClient()
{
    previousWebLogClientRequestMillis = millis();
    clientRequestedWebLoger = true;
}