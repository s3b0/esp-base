#include "dhtmanager.h"
#include "printmanager.h"
#include "utils.h"
#include <DHT.h>

DhtData readTempHumVal(DHT *dhtSensor, const int sensordId)
{
    struct DhtData dhtData;
    dhtData.temperature.id = sensordId;
    dhtData.humidity.id = sensordId;

    dhtData.temperature.value = dhtSensor->readTemperature();
    dhtData.humidity.value = dhtSensor->readHumidity();
    if (isnan(dhtData.temperature.value) && isnan(dhtData.humidity.value))
    {
        printFormat("DHT sensor id %d not connected or waiting for data.", WARNING, sensordId);
    }
    else
    {
        if (!isnan(dhtData.temperature.value))
            addValuesToTemperatureSensors(dhtData.temperature);
        if (!isnan(dhtData.humidity.value))
            addValuesToHumiditySensors(dhtData.humidity);
    }
    return dhtData;
}

DhtData getAverageTempHum(const DhtData first, const DhtData second)
{
    DhtData average;
    bool tempError = false, humidityError = false;
    if (isnan(first.temperature.value) && !isnan(second.temperature.value))
    {
        print("First DHT11 has temperature set to NAN!", WARNING);
        average.temperature.value = second.temperature.value;
        tempError = true;
    }
    else if (!isnan(first.temperature.value) && isnan(second.temperature.value))
    {
        print("Second DHT11 has temperature set to NAN!", WARNING);
        average.temperature.value = first.temperature.value;
        tempError = true;
    }
    else if (isnan(first.temperature.value) && isnan(second.temperature.value))
    {
        print("Both DHT11s has temperature set to NAN! Setting average temp to 0!", ERROR);
        average.temperature.value = 0;
        tempError = true;
    }
    else
    {
        printFormat("All dhts has temperature id %d=%.2fC %d=%.2fC", INFO, first.temperature.id, first.temperature.value, second.temperature.id, second.temperature.value);
        tempError = false;
        ;
    }
    if (isnan(first.humidity.value) && !isnan(second.humidity.value))
    {
        print("First DHT11 has humidity set to NAN!", WARNING);
        average.humidity.value = second.humidity.value;
        humidityError = true;
    }
    else if (!isnan(first.humidity.value) && isnan(second.humidity.value))
    {
        print("Second DHT11 has humidity set to NAN!", WARNING);
        average.humidity.value = first.humidity.value;
        humidityError = true;
    }
    else if (isnan(first.humidity.value) && isnan(second.humidity.value))
    {
        print("Both DHT11s has humidity set to NAN! Setting average humidity to 0!", ERROR);
        average.humidity.value = 0;
        humidityError = true;
    }
    else
    {
        printFormat("All dhts has humidity id %d=%.2f%% %d=%.2f%%", INFO, first.humidity.id, first.humidity.value, second.humidity.id, second.humidity.value);
        humidityError = false;
    }
    if (!tempError)
    {
        average.temperature.value = (first.temperature.value + second.temperature.value) / 2.0f;
        printFormat("Average temp = %.2f C", DEBUG, average.temperature.value);
    }
    if (!humidityError)
    {
        average.humidity.value = (first.humidity.value + second.humidity.value) / 2.0f;
        printFormat("Average hum = %.2f %%", DEBUG, average.humidity.value);
    }
    average.temperature.id = 0;
    average.temperature.isAverage = true;
    average.humidity.id = 0;
    average.humidity.isAverage = true;

    setAverageValueForTemperatureSensors(average.temperature);
    setAverageValueForHumiditySensors(average.humidity);
    return average;
}