#include <ESP8266WebServer.h>
#include <webservermanager.h>
#include <printmanager.h>
#include <ESP8266mDNS.h>
#include <fsmanager.h>
#include <appdatamanager.h>
#include <configmanager.h>
#include "utils.h"
#include "actionmanager.h"
#include "sensorsmanager.h"
#include "deviceinfomanager.h"
#include <uri/UriBraces.h>

ESP8266WebServer *WebServer;
#define JSON_ARG_NAME "plain"
#define RESPONSE_TO_CLIENT "{\"%s\" : \"%s\"}"
#define MIME_JSON "application/json"
#define MIME_TEXT "text/plain"
#define MIME_HTML "text/html"
#define MIME_CSS "text/css"
#define MIME_JAVASCRIPT "text/javascript"
#define MIME_ICO "image/x-icon"
#define SLASH "/"
//pages, data acces points, css and so on
const char *CSS_MINI_FILE = "mini-default.min.css";
const char *CSS_MAIN_FILE = "stylesheet.css";
const char *JAVASCRIPT_FILE = "script.js";
const char *CSS_ROOT_CONTEXT = "/css";
const char *JAVASCRIPT_ROOT_CONTEXT = "/javascript";
const char *APP_ROOT_CONTEXT = "/app";
const char *CONFIG_ROOT_CONTEXT = "/config";
const char *DATA_READ_CONTEXT = "read";
const char *DATA_WRITE_CONTEXT = "write";

String readRequestUri()
{
    //uri requested by client - use it to identify page that should be served
    return getWebServer()->uri();
}

void streamFileToClient(File page, const String mime)
{
    getWebServer()->streamFile(page, mime);
}
// Gets combines passed data to full URI
/*String getFullContext(const char *rootContext, const char *uri)
{
    int bufferLength = sizeof(char) * (sizeof(rootContext) + sizeof(uri) + 1);
    char buffer[bufferLength];
    strcat(buffer, rootContext);
    strcat(buffer, SLASH);
    strcat(buffer, uri);
    String str = String(buffer);
    str.trim();
    return str;
}*/
void printMsgToClient(const int httpCode, const char *status, const char *message)
{
    int bufferLength = sizeof(char) * (sizeof(message) + sizeof(status) + sizeof(RESPONSE_TO_CLIENT) + 1);
    char buffer[bufferLength];
    sprintf(buffer, RESPONSE_TO_CLIENT, status, message);
    getWebServer()->send(httpCode, MIME_JSON, buffer);
}
// serves pages for client request
void servePage()
{
    String pageName = "index.html";
    const String uri = readRequestUri();
    String mime = MIME_HTML;
    if (uri.equals("/app")) //clients request for config page
    {
        pageName = "app.html";
    }
    else if (uri.equals("/config"))
    {
        pageName = "config.html";
    }
    else if (uri.equals("/css/mini-default.min.css"))
    {
        pageName = CSS_MINI_FILE;
        mime = MIME_CSS;
    }
    else if (uri.equals("/css/stylesheet.css"))
    {
        pageName = CSS_MAIN_FILE;
        mime = MIME_CSS;
    }
    else if (uri.equals("/javascript/script.js"))
    {
        pageName = JAVASCRIPT_FILE;
        mime = MIME_JAVASCRIPT;
    }
    else if (uri.equals("/favicon.ico"))
    {
        pageName = "favicon.ico";
        mime = MIME_ICO;
    }
    else if (uri.equals("/state"))
    {
        pageName = "state.html";
    }
    else if (uri.equals("/sensors"))
    {
        pageName = "sensors.html";
    }
    else if (uri.equals("/device"))
    {
        pageName = "device.html";
    }
    else if (uri.equals("/log"))
    {
        pageName = "log.html";
    }
    else if(uri.equals("/javascript/chart.js"))
    {
        pageName = "chart.js";
        mime = MIME_JAVASCRIPT;
    }

    File page = getFile(pageName);
    if (page)
    {
        streamFileToClient(page, mime);
        page.close();
    }
    else
    {
        String error404Page = "<DOCTYPE html><html>Server couldn't find file you were requesting! Check your data folder and upload file(" + pageName + ")<html/>";
        getWebServer()->send(404, MIME_HTML, error404Page);
    }
}

void readData()
{
    const String uri = readRequestUri();
    if (uri.equals("/app/read")) //clients request for config page
    {
        streamFileToClient(readAppData(), MIME_JSON);
        return;
    }
    else if (uri.equals("/config/read"))
    {
        streamFileToClient(readConfigData(), MIME_JSON);
        return;
    }
    else if (uri.equals("/sensors/read"))
    {
        getWebServer()->send(200, MIME_JSON, readSensorsData());
    }
    else if (uri.equals("/device/read"))
    {
        const char *deviceInfo = getDeviceInfo();
        getWebServer()->send(200, MIME_JSON, deviceInfo);
        free((char *)deviceInfo);
    }
    else if (uri.equals("/log/sync"))
    {
        const char *loggerInfo = getWebLoggerInfo();
        getWebServer()->send(200, MIME_JSON, loggerInfo);
        free((char *)loggerInfo);
    }
    else if (uri.equals("/log/read")) // jeśli nie zapytam co interwał wyłącz weblogera
    {
        enablePrintingWebLogerForWebClient();
        const char *webLogs = printWebLogs();
        getWebServer()->send(200, MIME_TEXT, webLogs);
        free((char *)webLogs);
    }
    printMsgToClient(400, "ERROR", "No server data to read!");
}

void writeData()
{
    int args = getWebServer()->args();
    if (args > 0)
    {
        for (int i = 0; i < args; i++)
        {
            String argName = getWebServer()->argName(i);
            String argValue = getWebServer()->arg(i);
            printFormat("Have arg name: %s, value: %s", DEBUG, argName.c_str(), argValue.c_str());
            if (strcmp(argName.c_str(), JSON_ARG_NAME) == 0) //we have full string with arguments from browser, heer will be json from form
            {
                const String uri = readRequestUri();
                bool allGood = false;
                if (uri.equals("/app/write")) //clients request for config page
                {
                    saveAppData(argValue);
                    allGood = true;
                }
                else if (uri.equals("/config/write"))
                {
                    saveConfigData(argValue);
                    allGood = true;
                }
                if (allGood)
                    printMsgToClient(200, "STATUS", "OK");
                else
                    printMsgToClient(400, "ERROR", "No uri found!");
                return;
            }
        }
    }
    printMsgToClient(400, "ERROR", "No client data!");
}

void reactOnPath()
{
    const String uri = readRequestUri();
    printFormat("URI: %s", DEBUG, uri.c_str());
    if (uri.equals("/state/reboot"))
    {
        printMsgToClient(200, "STATUS", "OK"); //don use flags - action take place to quick!
        delay(2000);                           //leve this for web GUI
        reboot();
    }
    else if (uri.equals("/state/deepSleep"))
    {
        printMsgToClient(200, "STATUS", "OK");
        delay(2000); //leve this for web GUI
        deepSleep();
    }
    printMsgToClient(400, "ERROR", "No client data!");
}

bool setupWebServer(IPAddress webServerAddress, String dnsName, int port)
{
    WebServer = new ESP8266WebServer(webServerAddress, port);
    getWebServer()->on("/", servePage);
    getWebServer()->on("/index.html", servePage);
    getWebServer()->on("/favicon.ico", servePage);
    getWebServer()->on("/css/mini-default.min.css", servePage);
    getWebServer()->on("/css/stylesheet.css", servePage);
    getWebServer()->on("/javascript/script.js", servePage);
    getWebServer()->on("/javascript/chart.js", servePage);
    getWebServer()->on("/app", servePage);
    getWebServer()->on("/app/read", readData);
    getWebServer()->on("/app/write", writeData);
    getWebServer()->on("/config", servePage);
    getWebServer()->on("/config/read", readData);
    getWebServer()->on("/config/write", writeData);
    getWebServer()->on("/state", servePage);
    //https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WebServer/examples/PathArgServer/PathArgServer.ino
    getWebServer()->on(UriBraces("/state/{}"), reactOnPath);
    getWebServer()->on("/sensors", servePage);
    getWebServer()->on("/sensors/read", readData);
    getWebServer()->on("/device", servePage);
    getWebServer()->on("/device/read", readData);
    getWebServer()->on("/log", servePage);
    getWebServer()->on("/log/sync", readData);
    getWebServer()->on("/log/read", readData);

    getWebServer()->begin();
    if (getWebServer())
    {
        printFormat("Web server is listening on port %d", INFO, port);
        if (MDNS.begin(dnsName, webServerAddress)) // Start the mDNS responder for simple dns
        {
            print("mDNS responder started", INFO);
            MDNS.addService("http", "tcp", port);
        }
        else
        {
            print("Error setting up MDNS responder!", ERROR);
        }
        return true;
    }
    return false;
}

ESP8266WebServer *getWebServer()
{
    if (!WebServer)
        print("Web server request but not aviable!", ERROR);
    return WebServer;
}

void shutDownWebServer()
{
    print("Shuting down web server", INFO);
    if (WebServer)
    {
        getWebServer()->close();
        getWebServer()->stop();
        free(WebServer);
    }
}
