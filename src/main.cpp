#include "settingsmanager.h"
#include "printmanager.h"
#include "fsmanager.h"
#include "wifimanager.h"
#include "utils.h"
#include "serialmanager.h"
#include "dhtmanager.h"
#include <Arduino.h>
#include "actionmanager.h"
#include "sensorsmanager.h"
#include "appdatamanager.h"
#include "deviceinfomanager.h"

#ifdef USE_OTA
#include "otamanager.h"
#endif

#ifdef USE_WEBSERVER
#include <webservermanager.h>
bool hasWebServer = false;
//only valid when in AP mode
const char *WebServerDnsName = "eps";
#endif

#define PIN_0_RESET_WIFI 0
#define PIN_12_DHT11 12
#define PIN_13_DHT11 13
#define PIN_4_SSR 4
#define PIN_5_SSR 5

unsigned long previousReadMillis = 0;
// reliable to 5 meters od CAT5 network cable
DHT dht1(PIN_12_DHT11, DHT11);
DHT dht2(PIN_13_DHT11, DHT11);
DhtData dht1Data;
DhtData dht2Data;

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  dht1.begin();
  dht2.begin();
  pinMode(PIN_12_DHT11, INPUT);
  pinMode(PIN_13_DHT11, INPUT);
  pinMode(PIN_4_SSR, OUTPUT);
  pinMode(PIN_5_SSR, OUTPUT);
  digitalWrite(PIN_4_SSR, LOW);
  digitalWrite(PIN_5_SSR, LOW);

  serialBegin();

  if (setupWiFi())
  {
#ifdef USE_OTA
    setupOTA();
#endif
#ifdef USE_WEBSERVER
    hasWebServer = setupWebServer(getLocalIP(), WebServerDnsName);
#endif
  }
}

void loop()
{
  setLogger(getLogData()); //check global log level

  if (isWifiConnected())
  {
#ifdef USE_OTA
    handleOTA();
#endif
#ifdef USE_WEBSERVER
    if (hasWebServer)
      getWebServer()->handleClient();
#endif
  }
  if (digitalRead(PIN_0_RESET_WIFI) == LOW)
    reset();

  //our delay :)
  unsigned long currentMillis = millis();
  if (currentMillis - previousReadMillis >= SENSOR_READ_DELAY)
  {
    // save the last time you blinked the LED
    previousReadMillis = currentMillis;
    dht1Data = readTempHumVal(&dht1, 1);
    dht2Data = readTempHumVal(&dht2, 2);
    DhtData dhtAverageData = getAverageTempHum(dht1Data, dht2Data);
    const float temperatureTreshold = getFloatValue(JSON_TEMPERATURE_SENSOR_KEY);
    const float humidityTreshold = getFloatValue(JSON_HUMIDITY_SENSOR_KEY);
    if (dhtAverageData.temperature.value > temperatureTreshold || dhtAverageData.humidity.value >= humidityTreshold)
    {
      digitalWrite(PIN_4_SSR, HIGH);
      digitalWrite(PIN_5_SSR, HIGH);
    }
    else
    {
      digitalWrite(PIN_4_SSR, LOW);
      digitalWrite(PIN_5_SSR, LOW);
    }
  }
}
