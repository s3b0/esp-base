#include <Arduino.h>
#include "settingsmanager.h"
#include "serialmanager.h"
#include "printmanager.h"
#include "utils.h"

void serialBegin()
{
    Serial.begin(115200);
    Serial.setTimeout(2000);
    print("Initializing serial");
    // Wait for serial to initialize.
    while (!Serial)
    {
        print(".", APPENDING);
    }
    print("Done");
}

void serialEnd()
{
    if (Serial)
    {
        print("Closing serial");
        Serial.end();
    }
}