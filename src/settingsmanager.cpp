#include "settingsmanager.h"
#include "wifimanager.h"
#include "appdatamanager.h"
#include "configmanager.h"
#include "printmanager.h"
#include "actionmanager.h"
#include "utils.h"

#define WEB_LOGERR_JSON_DATA_FORMAT "{\"LOGGER_DATA\": {\"delay\": %d, \"separator\": \"%c\"}}"

void reset()
{
    print("Reseting device", INFO);
    resetWifiSettings();
    resetConfig();
    resetAppData();
    ESP.eraseConfig();
    reboot();
}

char *getWebLoggerInfo()
{
    const int jsonLength = strlen(WEB_LOGERR_JSON_DATA_FORMAT) + intLength(WEB_LOGER_READ_DELAY) + WEB_LOG_LINE_SEPARATOR;
    char *jsonObject = (char *)malloc(jsonLength * sizeof(char));
    sprintf(jsonObject, WEB_LOGERR_JSON_DATA_FORMAT, WEB_LOGER_READ_DELAY, WEB_LOG_LINE_SEPARATOR);
    return jsonObject;
}