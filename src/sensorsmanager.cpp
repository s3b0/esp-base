#include "sensorsmanager.h"
#include "dhtmanager.h"
#include "utils.h"
#include "settingsmanager.h"
/**
 * Beginig of json sensr string
 */
#define SENSOR_JSON_HEADER "{\"SENSOR_DATA\": ["
/**
 * End of sensor json string
 */
#define SENSOR_JSON_END "]}"
/**
 * This is format for all sensor, it will fill json array in SENSPR_DATA object (in json)
 */
#define SENSOR_JSON_OBJECT_FORMAT "{\"id\": %d,\"name\": \"%s\",\"value\": %.2f,\"unit\": \"%c\",\"type\": \"%s\",\"isaverage\":%s},"
/**
 * Interan array for temparature sensors
 */
TemperatureSensor temperatureSensors[SENSORS_MAX_COUNT];
/**
 * Stores average values for temperature sensors
 */
TemperatureSensor averageTemperatureSensors;
/**
 * Interan array for humidity sensors
 */
HumiditySensor humiditySensors[SENSORS_MAX_COUNT];
/**
 * Stores average values for humidity sensors
 */
HumiditySensor averageHumiditySensors;
/**
 * Converts sensor data to string representation of json object
 * @param temperatureSensor temperature sensor data
 * @param json json object to appned temperatures sensor data
 */
void temperatureSensorDataToJsonString(TemperatureSensor temperatureSensor, String *json)
{
    const char *booleanAsString = booleanToString(temperatureSensor.isAverage);
    const int jsonLength = strlen(SENSOR_JSON_OBJECT_FORMAT) + intLength(temperatureSensor.id) + temperatureSensor.name.length() +
                           intLength(temperatureSensor.value) + 3 + strlen(temperatureSensor.type) + strlen(booleanAsString) + 1;
    char *jsonObject = (char *)malloc(jsonLength * sizeof(char));
    sprintf(jsonObject, SENSOR_JSON_OBJECT_FORMAT, temperatureSensor.id, temperatureSensor.name.c_str(),
            temperatureSensor.value, temperatureSensor.unit, temperatureSensor.type, booleanAsString);
    free((char *)booleanAsString);
    json->concat(jsonObject);
    free(jsonObject);
}
/**
 * Converts sensor data to string representation of json object
 * @param humiditySensor humidity sensor data
 * @param json json object to appned humidity sensor data
 */
void humiditySensorDataToJsonString(HumiditySensor humiditySensor, String *json)
{
    const char *booleanAsString = booleanToString(false);
    const int jsonLength = strlen(SENSOR_JSON_OBJECT_FORMAT) + intLength(humiditySensor.id) + humiditySensor.name.length() +
                           intLength(humiditySensor.value) + 3 + strlen(humiditySensor.type) + strlen(booleanAsString) + 1;
    char *jsonObject = (char *)malloc(jsonLength * sizeof(char));
    sprintf(jsonObject, SENSOR_JSON_OBJECT_FORMAT, humiditySensor.id, humiditySensor.name.c_str(),
            humiditySensor.value, humiditySensor.unit, humiditySensor.type, booleanAsString);
    free((char *)booleanAsString);
    json->concat(jsonObject);
    free(jsonObject);
}

const String readSensorsData()
{
    String sensorsJson(SENSOR_JSON_HEADER);
    for (int i = 0; i < SENSORS_MAX_COUNT; i++)
    {
        TemperatureSensor tmpTs = temperatureSensors[i];
        HumiditySensor tmpHs = humiditySensors[i];
        if (!isnan(tmpTs.value))
        {
            temperatureSensorDataToJsonString(tmpTs, &sensorsJson);
        }
        if (!isnan(tmpHs.value))
        {
            humiditySensorDataToJsonString(tmpHs, &sensorsJson);
        }
    }
    if (!isnan(averageTemperatureSensors.value))
    {
        temperatureSensorDataToJsonString(averageTemperatureSensors, &sensorsJson);
    }
    if (!isnan(averageHumiditySensors.value))
    {
        humiditySensorDataToJsonString(averageHumiditySensors, &sensorsJson);
    }
    sensorsJson = sensorsJson.substring(0, sensorsJson.length() - 1);
    sensorsJson += SENSOR_JSON_END;
    return sensorsJson;
}

void addValuesToTemperatureSensors(const TemperatureSensor temperatureSensor)
{
    for (int i = 0; i < SENSORS_MAX_COUNT; i++)
    {
        TemperatureSensor tmpTs = temperatureSensors[i];
        if (!tmpTs.id) // if no id put on first position
        {
            temperatureSensors[i] = temperatureSensor;
            return;
        }
        else if (tmpTs.id == temperatureSensor.id) //if we have this sensor already put it on its position
        {
            temperatureSensors[i] = temperatureSensor;
            return;
        }
    }
}
void setAverageValueForTemperatureSensors(const TemperatureSensor temperatureSensor)
{
    averageTemperatureSensors = temperatureSensor;
    averageTemperatureSensors.isAverage = true;
    averageTemperatureSensors.name = "Average value for all temperature sensors";
}
void addValuesToHumiditySensors(const HumiditySensor humiditySensor)
{
    for (int i = 0; i < SENSORS_MAX_COUNT; i++)
    {
        HumiditySensor tmpHs = humiditySensors[i];
        if (!tmpHs.id) // if no id put on first position
        {
            humiditySensors[i] = humiditySensor;
            return;
        }
        else if (tmpHs.id == humiditySensor.id) //if we have this sensor already put it onits position
        {
            humiditySensors[i] = humiditySensor;
            return;
        }
    }
}
void setAverageValueForHumiditySensors(const HumiditySensor humiditySensor)
{
    averageHumiditySensors = humiditySensor;
    averageHumiditySensors.isAverage = true;
    averageHumiditySensors.name = "Average value for all humidity sensors";
}