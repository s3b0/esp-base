#include "wifimanager.h"
#include <ESP8266WiFi.h> //ESP8266 Core WiFi Library (you most likely already have this in your sketch)
#include "configmanager.h"
#include "printmanager.h"
#include "utils.h"
#include "settingsmanager.h"
#include "actionmanager.h"

// are we connected to WIFI
bool areWeConnected = false;
// AP clients
int connectedClients = 0;
IPAddress localIpAddress;
void resetWifiSettings();
void wifiManualSetup(WIFI_DATA wifiData, NET_DATA netData);

int getConnectedClients()
{
    if(WiFi.getMode() == WIFI_AP)
    {
        return connectedClients;
    }

    print("Wifi in STA mode - no connected client info avaible.", WARNING);
    return 0;
}

void onStationConnected(const WiFiEventSoftAPModeStationConnected& evt) {
  print("Station connected: ");
  print(macToString(evt.mac));
  ++connectedClients;
}

void onStationDisconnected(const WiFiEventSoftAPModeStationDisconnected& evt) {
  print("Station disconnected: ");
  print(macToString(evt.mac));
  --connectedClients;
}

bool setupWiFi()
{
    // Disable the WiFi persistence. The ESP8266 will not load and save WiFi settings in the flash memory.
    //WiFi.persistent(false); // not wroking with reset settings of WiFiManager
    // flag connect state
    areWeConnected = false;

    WIFI_DATA wifiData = getWifiData();
    NET_DATA netData = getNetData();

    wifiManualSetup(wifiData, netData);

    areWeConnected = true;
    print("Connected");
    switch (WiFi.getMode())
    {
    case WIFI_AP:
        localIpAddress = WiFi.softAPIP();
        print("AP ip: " + localIpAddress.toString(), INFO);
        WiFi.onSoftAPModeStationConnected(&onStationConnected);
        WiFi.onSoftAPModeStationDisconnected(&onStationDisconnected);
        break;
    case WIFI_STA:
        localIpAddress = WiFi.localIP();
        print("Device ip: " + localIpAddress.toString(), INFO);
        print("Gateway: " + WiFi.gatewayIP().toString(), INFO);
        print("Netmask: " + WiFi.subnetMask().toString(), INFO);
        break;
    default:
        print("No mode set!", ERROR);
        areWeConnected = false;
    }

    return areWeConnected;
}

void wifiManualSetup(WIFI_DATA wifiData, NET_DATA netData)
{
    bool error = false;
    if (!wifiData.mode)
    {
        print("No manual mode provided in config.json!", ERROR);
        error = true;
    }
    if (!wifiData.psk)
    {
        print("No psk provided!", ERROR);
        error = true;
    }
    if (!wifiData.ssid)
    {
        print("No ssid provided!", ERROR);
        error = true;
    }
    if (error)
    {
        print("Fix above errors! Wifi will not be aviable!", ERROR);
        return;
    }

    WiFi.forceSleepWake(); // not wroking with reset settings of WiFiManager

    print("Mode: " + String(wifiData.mode) + ", SSID: " + wifiData.ssid + ", PSK: " + (wifiData.psk ? "Available" : "Not available"), INFO, false);
    int modeLength = sizeof(wifiData.mode);
    if (strncmp(wifiData.mode, "AP", modeLength) == 0) //config as AP
    {
        print(", CHANNEL: " + String(wifiData.channel) + ", IS HIDDEN: " + wifiData.hidden + ", MAX CLIENT COUNT: " + wifiData.max_wifi_client_count, APPENDING);
        WiFi.mode(WIFI_AP);
        WiFi.enableAP(true);
        IPAddress ip, gw, nm;
        ip.fromString(netData.staticIp);
        gw.fromString(netData.staticGw);
        nm.fromString(netData.staticNm);
        WiFi.softAPConfig(ip, gw, nm);
        WiFi.softAP(wifiData.ssid, wifiData.psk, wifiData.channel, wifiData.hidden, wifiData.max_wifi_client_count);
    }
    else if (strncmp(wifiData.mode, "STA", modeLength) == 0) //config as STA
    {
        WiFi.mode(WIFI_STA);
        WiFi.enableSTA(true);
        WiFi.begin(wifiData.ssid, wifiData.psk);
        print("Connecting", DEBUG, false);
        while (WiFi.status() != WL_CONNECTED)
        {
            print(".", APPENDING, false);
        }
        print("", APPENDING);
    }
}

// Resets all settings(wipe it out)
void resetWifiSettings()
{
    print("Wiping all wifi settings...");
    WiFi.disconnect(true);
    print("Done. Restarting...");
}

// Disconnects Wifi - prepare for sleep
void disconnectWifi()
{
    if (areWeConnected)
    {
        print("Disconnecting from ", APPENDING);
        print(WiFi.softAPSSID());
        WiFi.disconnect(true);
        print("");
    }
    //power safe
    WiFi.mode(WIFI_OFF);
    WiFi.forceSleepBegin();
    // falg connection state
    areWeConnected = false;
}

bool isWifiConnected()
{
    return areWeConnected;
}

IPAddress getLocalIP()
{
    return localIpAddress;
}