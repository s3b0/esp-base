# What is it
I need base for my future project with ability to easy configure and monitor ESP 12-F via WWW so i made this. 

# What it can do
It provides features as:
* configure:
    * WiFi settings (STA and AP)
    * Logging level
    * Sleep time and type (not fully done)
    * MQTT server client setting (not done at this moment at all)
* monitor:
    * memory (as chart)
    * view info about board (for now is as plain text)
    * appliaction logs (need some improvments)
* restart or sleep device
* easy gui managment HTML+JavaScript via web pages (server is builtin into device)
* scripts for developers to easy erase and deploy gui to device (see .vscode/tasks.json)

# Some details
+ I stick to C as i could
+ Framework for this app is arduino@platformio (https://platformio.org/)
+ for webserwer i use ESP8266WebServer
+ backend use JSON and REST
+ frontend has been written in plain HTML5 with javascript
+ css framework: mini.css (https://minicss.org/)
+ chart framework: chart.js (https://www.chartjs.org/)
+ right now app is targeted for DTH11 sensors (as example) but i think that it is made(app) to allow easy chage for something you want
+ i like KISS rule (https://en.wikipedia.org/wiki/KISS_principle) so i tried to done this code according to it
+ app configuration (i call it this way) is done via JSON object on client side (see app.html APP_DATA) - only configuration objec is dependend on structures in c code
+ initial configuration default values are fixed in configmanager.h (you can change them there)

# What needs to be added
- STA-AP mode
- WIFI scanner for easy pick WIFI in STA(STA-AP) mode
- autoscrolling of webloger is fixed, i need to add posibilyty to pause it
- info about board need some styling
- sleep mode need some more work
- mqtt client

## Notice
I'm not pro in C so any advice/constructive criticism is appreciated ;). Recommended ide is Visual Studio Code
