#!/bin/bash
set -e

WEB_DIR='./web'
DATA_DIR='./data'

rm -rf "${DATA_DIR}"
mkdir "${DATA_DIR}"

#first remove comments then minimize
for file in $(ls ${WEB_DIR}/*.html)
do
    echo "Minifing html: ${file}"
    cpp -undef -P ${file} | sed -e 's/^[ \t]*//g' -e ':a;N;$!ba;s/\n\s*//g' > ${DATA_DIR}/$(basename ${file})
done
for file in $(ls ${WEB_DIR}/css/*.css)
do
    echo "Minifing css: ${file}"
    cp -f ${file} ${DATA_DIR}/$(basename ${file})
done
for file in $(ls ${WEB_DIR}/javascript/*.js)
do
    echo "Minifing javascript: ${file}"
    cpp -undef -P ${file} | sed -e 's/^[ \t]*//g' -e ':a;N;$!ba;s/\n\s*//g' > ${DATA_DIR}/$(basename ${file})
done
for file in $(ls ${WEB_DIR}/*.ico)
do
    echo "Copying ico: ${file}"
    cp -f ${file} ${DATA_DIR}/$(basename ${file})
done

echo "Uploading fs"
platformio run --target uploadfs