var isExternalCssLoaded = true;

function enableFormElements(form) {
    let elements = form.elements;
    for (let i = 0, len = elements.length; i < len; ++i) {
        enableElement(elements[i]);
    }
}

function disableFormElements(form) {
    let elements = form.elements;
    for (let i = 0, len = elements.length; i < len; ++i) {
        disableElement(elements[i]);
    }
}

function disableElement(element) {
    element.disabled = true;
}

function enableElement(element) {
    element.disabled = false;
}

function printToastMessage(toast, level, message) {
    if (isExternalCssLoaded) {
        toast.style.display = "none";
        toast.innerHTML = message;
        toast.style.display = "block";
        setTimeout(function () {
            toast.style.display = "none";
            toast.innerHTML = "";
        }, 3000);
    }
}

function showProgress(progress, progressBackground) {
    progressBackground.style.display = "block";
    progress.style.display = "block";
}
function hideProgress(progress, progressBackground) {
    progress.style.display = "none";
    progressBackground.style.display = "none";
}

function showLoader(loader, body) {
    body.style.display = "none";
    loader.style.display = "block";
}
async function loadLoaderAsync() {
    showLoader(loader, body);
}
function hideLoader(loader, body) {
    loader.style.display = "none";
    body.style.display = "block";
}

function setRadioValue(radioGroup, radioValue) {
    radioGroup.forEach((item, index) => {
        if (`${item.value}` == `${radioValue}`) { //always comapre booleans as strings in javascript !!!!
            item.checked = true;
            return;
        }
    });
}
function getRadioValue(radioGroup) {
    for (let i = 0; i < radioGroup.length; i++) {
        let item = radioGroup[i];
        if (item.checked) {
            return item.value;
        }
    }
    return "";
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function getValueType(p) {
    if (Array.isArray(p)) {
        return 'array';
    } else if (typeof p == 'string') {
        return 'string';
    } else if (p != null && typeof p == 'object') {
        return 'object';
    }
    return 'other';
}

function getCurrenHourWithMinutes()
{
    let date = new Date();
    let hour = date.getHours();
    let minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    let seconds = date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds();
    return `${hour}:${minutes}.${seconds}`;
}
function getCheckboxBooleanValue(checkboxInput) {
    if(checkboxInput.checked){
        checkboxInput.value = "true";
        return checkboxInput.value;
    }
    checkboxInput.value = "false";
    return checkboxInput.value;
}
function setCheckboxBooleanValue(checkboxInput, checkboxValue) {
    if(checkboxValue) {
        checkboxInput.value = "true";
        checkboxInput.checked = true;
        return;
    }
    checkboxInput.value = "false";
    checkboxInput.checked = false;
}
/*
//doesn't work as expected - huge delays on loading styles!
function loadExternalCss() {
    let cssFileName = "/css/mini-default.min.css";
    let cssId = "mainCss";
    let ifConnected = window.navigator.onLine;
    if (ifConnected) {
        console.log('Internet available');
        cssFileName = "https://cdn.rawgit.com/Chalarangelo/mini.css/v3.0.1/dist/mini-default.min.css";
    } else {
        console.log('Internet not available');
    }
    if (!document.getElementById(cssId)) {
        let head = document.getElementsByTagName('head')[0];
        let link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = cssFileName;
        link.media = 'all';
        link.onload = function () {
            console.log(`Loaded ${cssFileName}`);
            let loader = document.querySelector('div[id="loader"]');
            let body = document.querySelector('div[id="body"]');
            hideLoader(loader, body);
            isExternalCssLoaded = true;
        };
        head.appendChild(link);
    }
}
*/
